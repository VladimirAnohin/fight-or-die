﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class UnitTesting : MonoBehaviour
{

    PlayerController playerController;
    EnemySkeleton enemy;

    [Test]
    public void PlayerControllerTestingGetDamage()
    {
        var player = new GameObject();
        playerController = player.AddComponent<PlayerController>();
        playerController.Hp = 150;
        playerController.GetDamage(5);
        Assert.AreEqual(145, playerController.Hp);

        playerController.GetDamage(145);
        Assert.AreEqual(0, playerController.Hp);
        Assert.IsTrue(playerController.IsDead);
    }

    [Test]
    public void EnemySkeletonTestingGetDamage()
    {
        var enemyGo = new GameObject();
        enemy = enemyGo.AddComponent<EnemySkeleton>();
        enemy.Hp = 100;
        enemy.GetDamage(5);
        Assert.AreEqual(95, enemy.Hp);

        enemy.GetDamage(95);
        Assert.AreEqual(0, enemy.Hp);
    }

    [Test]
    public void MoneyViewTesting()
    {
        var view = new GameObject();
        var moneyView = view.AddComponent<MoneyView>();
        var txt = new GameObject();
        UnityEngine.UI.Text text = txt.AddComponent<UnityEngine.UI.Text>();
        moneyView.money = text;

        moneyView.SetMoney(500);
        Assert.AreEqual(500, System.Convert.ToInt16(moneyView.money.text));
    }
}
