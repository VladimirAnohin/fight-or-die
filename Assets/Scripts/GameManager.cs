﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    public ArenaSelector arenaSelector;
    public CharacterSelector characterSelector;

    public ArenaController[] areas;
    public PlayerController[] characters;

    private PlayerController currentPlayer;
    private ArenaController currentArea;

    private Dictionary<string, ArenaController> arenasDictionary;
    private Dictionary<string, PlayerController> charactersDictionary;

    private string characterName;

    private void Awake()
    {
        Debug.Log("Awake");
        arenasDictionary = new Dictionary<string, ArenaController> { { "Necropolis", areas[0] }, { "Ruins", areas[1] } };
        charactersDictionary = new Dictionary<string, PlayerController> { { "Arkadiy", characters[0] }, { "SmallHero", characters[1] } };
        DontDestroyOnLoad(this.gameObject);
    }

    public void StartGame()
    {
        if (Time.timeScale == 0)
            Time.timeScale = 1;

        StartCoroutine(StartGameCoroutine());
    }

    private IEnumerator StartGameCoroutine()
    {
        yield return StartCoroutine(InstCurrentPlayer());
        yield return StartCoroutine(InstCurrentArena());
        /*SceneManager.sceneLoaded += (Scene scene, LoadSceneMode mode) => {
            StartCoroutine(InstCurrentPlayer());
            StartCoroutine(InstCurrentArena());
        };*/

        //yield return StartCoroutine(OpenLevelScene());
    }

    private IEnumerator OpenLevelScene()
    {
        string arenaName = arenaSelector.GetCurrentArea();
        SceneManager.LoadScene(arenaName + "Scene");
        yield return null;
    }
    
    /*private IEnumerator InstCurrentArena()
    {
        string arenaName = arenaSelector.GetCurrentArea();

        Debug.Log(arenasDictionary[arenaName]);
        //if (arenasDictionary.ContainsKey(arenaName))
            //currentArea = arenasDictionary[arenaName];

        currentArea = FindObjectOfType<ArenaController>();
        currentArea.InstArena(currentPlayer);

        yield return null;
    }

    private IEnumerator InstCurrentPlayer()
    {
        string characterName = characterSelector.GetSelectedPlayer();
        Debug.Log(characterName);
        if (charactersDictionary.ContainsKey(characterName))
            currentPlayer = charactersDictionary[characterName];

        yield return null;
    }*/

    private IEnumerator InstCurrentArena()
    {
        string arenaName = arenaSelector.GetCurrentArea();

        Debug.Log(arenasDictionary[arenaName]);
        if (arenasDictionary.ContainsKey(arenaName))
            currentArea = arenasDictionary[arenaName];

        currentArea.gameObject.SetActive(true);
        currentArea.InstArena(currentPlayer);

        yield return null;
    }

    private IEnumerator InstCurrentPlayer()
    {
        string characterName = characterSelector.GetSelectedPlayer();
        Debug.Log(characterName);
        if (charactersDictionary.ContainsKey(characterName))
            currentPlayer = charactersDictionary[characterName];

        yield return null;
    }

    public void GameOver()
    {
        StartCoroutine(GameOverCoroutine());
    }

    private IEnumerator GameOverCoroutine()
    {
        yield return new WaitForSeconds(2f);
        MenuManager.inst.GameOver();
        Time.timeScale = 0;
    }

    public void RestartGame()
    {
        currentArea.ClearPlayer();
        currentArea.ClearNpc();
        StartGame();
    }

    public void EndGame()
    {
        currentArea.ClearPlayer();
        currentArea.ClearNpc();
        currentArea.gameObject.SetActive(false);
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
    }
}
