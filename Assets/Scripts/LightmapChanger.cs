﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightmapChanger : MonoBehaviour
{
    public Texture2D[] text;

    [ContextMenu("Change")]
    void Change()
    {
        LightmapData[] lightmapData = new LightmapData[5];

        lightmapData[0] = new LightmapData();
        lightmapData[0].lightmapColor = Resources.Load("Lightmap-0_comp_light", typeof(Texture2D)) as Texture2D;
        lightmapData[1] = new LightmapData();
        lightmapData[1].lightmapColor = Resources.Load("Lightmap-1_comp_light", typeof(Texture2D)) as Texture2D;
        lightmapData[2] = new LightmapData();
        lightmapData[2].lightmapColor = Resources.Load("Lightmap-2_comp_light", typeof(Texture2D)) as Texture2D;
        lightmapData[3] = new LightmapData();
        lightmapData[3].lightmapColor = Resources.Load("Lightmap-3_comp_light", typeof(Texture2D)) as Texture2D;
        lightmapData[4] = new LightmapData();
        lightmapData[4].lightmapColor = Resources.Load("Lightmap-4_comp_light", typeof(Texture2D)) as Texture2D;

        LightmapSettings.lightmaps = lightmapData;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
