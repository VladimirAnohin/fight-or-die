﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputController : MonoBehaviour
{
    private PlayerController playerController;

    private float timer = 0;

    private void Awake()
    {
        playerController = GetComponent<PlayerController>();
    }

    void FixedUpdate()
    {
        timer += Time.deltaTime;

#if UNITY_EDITOR
        float v = Input.GetAxis("Vertical");
        float h = Input.GetAxis("Horizontal");

        float a = Input.GetAxis("Fire1");
        float b = Input.GetAxis("Fire2");

#elif UNITY_ANDROID
        Vector3 angle = JoystickMovingController.inst.GetDirection() * -1;

        float v = angle.z;
        float h = angle.x;

        float a = JoystickRotateController.inst.GetAtackButton();
        float b = JoystickRotateController.inst.GetBlockButton();
#endif
        playerController.Move(h, v);
        playerController.Rotate();
        playerController.Atack(a, timer);
        playerController.Block(b);

    }
}
