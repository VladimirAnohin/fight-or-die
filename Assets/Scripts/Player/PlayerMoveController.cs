﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveController : MonoBehaviour
{
    public float speed;
    public float rotationSpeed;

    private Vector3 movement;
    private Rigidbody playerRigidbody;

    private void Start()
    {
        playerRigidbody = GetComponent<Rigidbody>();
    }

    public void Move(float h, float v)
    {
        movement.Set(h, 0, v);
        movement = movement.normalized * Time.deltaTime * speed;
        playerRigidbody.MovePosition(transform.position + movement);
    }

    public void Rotate()
    {
#if UNITY_EDITOR
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit floorHit;

        if (Physics.Raycast(camRay, out floorHit))
        {
            Vector3 viewDirection = floorHit.point - transform.position;

            viewDirection.y = 0;

            Quaternion newRotation = Quaternion.LookRotation(viewDirection);

            playerRigidbody.MoveRotation(newRotation);
        }

#elif UNITY_ANDROID
        Vector3 angle = JoystickMovingController.inst.GetDirection() * -1;

        if (angle == Vector3.zero)
            return;

        Quaternion newRotation = Quaternion.LookRotation(angle);
        playerRigidbody.MoveRotation(newRotation);
#endif
    }
}
