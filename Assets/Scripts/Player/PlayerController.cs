﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;
using UnityEngine.TestTools;

public class PlayerController : MonoBehaviour
{
    [Tooltip("ActiveWeapon")]
    public Weapon activeWeapon;

    public Weapon[] availableWeapons;
    private Weapon currentWeapon;

    private PlayerUI playerUI;
    private AnimationController animationController;
    private PlayerMoveController playerMoveController;

    [SerializeField]
    private int hp;
    public int Hp { get { return hp; } set { hp = value; } }

    [SerializeField]
    private int damage;
    private bool isDead = false;
    public bool IsDead { get { return isDead; } }
    private bool blocked = false;

    private float m_MaxDistance;

    private Collider m_Collider;

    public void Awake()
    {
        animationController = GetComponent<AnimationController>();
        playerMoveController = GetComponent<PlayerMoveController>();
        playerUI = FindObjectOfType<PlayerUI>();
        playerUI.SetFullIndicators();
        InitWeapon();
    }

    void Start()
    {
        //Choose the distance the Box can reach to
        m_MaxDistance = 1f;
        m_Collider = GetComponent<Collider>();
    }

    void FixedUpdate()
    {
        var hits = Physics.SphereCastAll(m_Collider.bounds.center, 1f, transform.forward, m_MaxDistance, 11);
        foreach (var hit in hits)
        {
            if(hit.collider != null && hit.collider.gameObject.tag.Equals("Npc") && canHit)
            {
                hit.collider.gameObject.SendMessage("GetDamage", damage);
            }
        }

        canHit = false;
    }

    private void InitWeapon()
    {
        foreach (var w in availableWeapons)
        {
            if (w == activeWeapon)
            {
                currentWeapon = w;
                break;
            }
        }
    }

    public void Atack(float a, float timer)
    {
        if (isDead)
            return;

        if (a > 0)
        {
            animationController.Atack(true);
            GetComponent<HeroSoundController>().PlaySource();
            //currentWeapon.TryHit(timer);
        }
        else
            animationController.Atack(false);
    }
    private bool canHit;
    //AnimAtack и StopAnimAtack вызываются с анимации атаки эвентом
    public void AnimAtack(int atackAnimation)
    {
        if (isDead)
            return;
        canHit = true;
        //currentWeapon.TryHit(timer);
    }

    public void StopAnimAtack(int atackAnimation)
    {
        if (isDead)
            return;

        //currentWeapon.StopHit(timer);
    }

    public void Move(float h, float v)
    {
        if (isDead)
            return;

        if (v > 0 || v < 0 || h > 0 || h < 0)
        {
            playerMoveController.Move(h, v);
            animationController.Walk(true);
            animationController.SetIdle(false);
        }
        else
        {
            animationController.Walk(false);
            animationController.SetIdle(true);
        }
    }

    public void Rotate()
    {
        if (isDead)
            return;

        playerMoveController.Rotate();
    }

    public void Block(float b)
    {
        if (b > 0)
        {
            animationController.Block(true);
            blocked = true;
        }
        else
        {
            animationController.Block(false);
            blocked = false;
        }
    }

    public void GetDamage(int damage)
    {
        if (blocked)
            return;

        float damagePercent = 0;

        if (hp != 0)
            damagePercent = (float)damage * 100 / (float)hp;

        hp = hp - damage;

        if (playerUI != null)
        {
            playerUI.SetPlayerHealth(damagePercent);
        }

        if (hp <= 0)
            Die();
    }

    public void Die()
    {
        isDead = true;

        if (animationController != null)
            animationController.Death(true);

        GameManager.inst.GameOver();
    }

    public void HitEffect()
    {
        currentWeapon.WeaponEffect();
    }
}
