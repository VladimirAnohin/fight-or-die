﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour {

    public Transform playerTarget;

    public float yPos;
    public float zPos;

    private Transform target;
    public Transform Target { get { return target; } set { target = value; } }

    private void Start()
    {
        playerTarget = FindObjectOfType<PlayerController>().transform;
    }

    void Update () {

        if (target == null)
            target = playerTarget;

        if (target == null)
            return;

        transform.position = new Vector3(target.position.x, target.position.y + yPos, target.position.z - zPos);

	}

    public void SetTarget(Transform target)
    {
        this.target = target;
    }
}
