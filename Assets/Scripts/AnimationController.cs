﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour {

    private Animator animator;

    private void Awake()
    {
        Init();
    }

    private void Init()
    {
        animator = GetComponent<Animator>();
    }

    public void Walk(bool walk)
    {
        animator.SetBool("Walk", walk);
    }

    public void SetIdle(bool idle)
    {
        animator.SetBool("Idle", idle);
    }

    public void Atack(bool atack)
    {
        animator.SetBool("Atack", atack);
    }

    public void Block(bool block)
    {
        animator.SetBool("Block", block);
    }

    public void Death(bool death)
    {
        animator.SetBool("Death", death);
    }
}
