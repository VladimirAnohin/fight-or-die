﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : Singleton<PlayerUI>
{
    public Image health;
    public Image energy;
    public Text killsCounter;
    public Text timer;

    public void SetPlayerHealth(float percentValue)
    {
        float newVal = health.fillAmount * (1.0f - percentValue / 100);

        health.fillAmount = newVal;
    }

    public void SetPlayerEnegry(int value)
    {
        energy.fillAmount -= value;
    }

    public void SetFullIndicators()
    {
        health.fillAmount = 100;
        energy.fillAmount = 100;
    }

    public void UpdateCounter(int counter)
    {
        killsCounter.text = counter.ToString();
    }

    public void UpdateTimer(float timer)
    {
        this.timer.text = timer.ToString();
    }
}
