﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuAnimator : MonoBehaviour
{
    public Action onEndAnimation;
    private Vector2 newPoint;

    private bool isAnimated;
    private float duration;

    private void Update()
    {
        if (!isAnimated)
            return;

        Animated();
    }

    private void Animated()
    {
        Vector3 newPosition = newPoint;
        transform.position = Vector2.MoveTowards(transform.position, newPoint, duration);

        if (transform.position == newPosition)
            if (onEndAnimation != null)
                onEndAnimation();
    }

    public void StartAnimation(bool state, Vector2 point, float duration)
    {
        isAnimated = true;
        newPoint = point;
        isAnimated = true;
        this.duration = duration;
    }
}
