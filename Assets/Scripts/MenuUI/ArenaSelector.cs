﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using System;

public class ArenaSelector : MonoBehaviour
{
    public Button backButton;
    public Button nextButton;
    public Button selectArea;
    public ArenaItemView arenaItemView;

    public Area[] areas;

    private Area currentArena;

    private int arenaIndex = 0;

    private void Start()
    {
        InstArea(areas[arenaIndex]);
    }

    public void OnClickBackArea()
    {
        arenaIndex--;
        InstArea(areas[arenaIndex]);
    }

    public void OnClickNextArea()
    {
        arenaIndex++;
        InstArea(areas[arenaIndex]);
    }

    public string GetCurrentArea()
    {
        if (currentArena != null)
            return currentArena.name;
        else
            return null;
    }

    private void InstArea(Area item)
    {
        if (currentArena != null)
            currentArena = null;

        currentArena = item;

        SetView();

        CheckButtons();
    }

    private void CheckButtons()
    {
        if (areas[arenaIndex] == areas.First())
            backButton.interactable = false;
        else
            backButton.interactable = true;


        if (areas[arenaIndex] == areas.Last())
            nextButton.interactable = false;
        else
            nextButton.interactable = true;

        string areaAchiev = currentArena.achievmentToUnlocked;
        Debug.Log(areaAchiev + " areaAchiev");
        if (string.IsNullOrEmpty(areaAchiev))
        {
            selectArea.interactable = true;
            return;
        }

        /*if (Auth.inst.achievmentsKeys.Contains(areaAchiev))
        {
            Debug.Log("Contains");
            selectArea.interactable = true;
        }
        else
        {
            Debug.Log("!Contains");
            selectArea.interactable = false;
        }*/
    }

    private void SetView()
    {
        arenaItemView.InstAreaView(currentArena.name, currentArena.iconArena);
    }
}

[Serializable]
public class Area
{
    public string name;
    public Sprite iconArena;
    public string achievmentToUnlocked;
}
