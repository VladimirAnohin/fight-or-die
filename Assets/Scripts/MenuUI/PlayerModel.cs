﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerModel : MonoBehaviour
{
    [SerializeField]
    private string name;
    public string Name { get { return name; } set { name = value; } }

    [SerializeField]
    private bool buyed;
    public bool Buyed { get { return buyed; } set { buyed = value; } }

    [SerializeField]
    private int price;
    public int Price { get { return price; } set { price = value; } }
}
