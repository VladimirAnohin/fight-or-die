﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMenuWindow {

    void Show();

    void Hide();

}
