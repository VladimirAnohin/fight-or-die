﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArenaItemView : MonoBehaviour
{
    public Text arenaName;
    public Image arenaIcon;

    public void InstAreaView(string name, Sprite icon)
    {
        arenaName.text = name;
        arenaIcon.sprite = icon;
    }
}
