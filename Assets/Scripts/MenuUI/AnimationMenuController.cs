﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationMenuController : MonoBehaviour
{
    public float duration;

    private Vector2 newPoint;
    private Vector2 showStartPosition;
    private MenuAnimator animator;
    private RectTransform rect;

    private bool isAnimated;
    private bool showed;
    private bool hided;

    private void Awake()
    {
        animator = GetComponent<MenuAnimator>();
        rect = GetComponent<RectTransform>();
        showStartPosition = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + Screen.width);
    }

    public void Show()
    {
        if (showed)
            return;

        gameObject.SetActive(true);
        showed = true;
        hided = false;

        int width = Screen.width;
        transform.position = showStartPosition;
        newPoint = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y - width);
        animator.StartAnimation(true, newPoint, duration);
    }

    public void Hide()
    {
        if (hided)
            return;

        hided = true;
        showed = false;
        int width = Screen.width;
        newPoint = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + width);
        animator.StartAnimation(true, newPoint, duration);
    }

    private void ShowHideGameObject()
    {
        if (gameObject.activeSelf)
            gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        animator.onEndAnimation -= ShowHideGameObject;
    }
}
