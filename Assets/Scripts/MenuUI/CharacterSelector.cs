﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelector : MonoBehaviour
{
    public Button backButton;
    public Button nextButton;
    public GameObject characterPlace;

    public Button selectButton;
    public Button buyButton;
    public Text price;

    public PlayerModel[] characters;

    public PlayerModel currentCharacter;

    private int index = 0;

    private void Start()
    {
        if (index == 0)
            PlayerPrefs.SetInt("Character" + characters[index].Name, 1);

        InstCharacter(characters[index]);
    }

    public void OnClickBackPlayer()
    {
        index--;
        InstCharacter(characters[index]);
    }

    public void OnClickNextPlayer()
    {
        index++;
        InstCharacter(characters[index]);
    }

    private void InstCharacter(PlayerModel model)
    {
        if (currentCharacter != null)
            Destroy(currentCharacter.gameObject);

        var character = Instantiate(model);
        character.Name = model.Name;

        int buyed = PlayerPrefs.GetInt("Character" + model.Name, 0);
        model.Buyed = buyed == 1 ? true : false;
        character.Buyed = model.Buyed;
        character.Price = model.Price;
        character.transform.SetParent(characterPlace.transform);
        character.transform.localPosition = new Vector3(0, 0, 0);
        currentCharacter = character;
        Debug.Log(currentCharacter.Name + "name");

        if (currentCharacter.Price > 0)
        {
            price.gameObject.SetActive(true);
            price.text = currentCharacter.Price.ToString();
        }
        else
            price.gameObject.SetActive(false);

        CheckButtons(currentCharacter.Buyed);
    }

    private void CheckButtons(bool buyed)
    {
        if (characters[index] == characters.First())
            backButton.interactable = false;
        else
            backButton.interactable = true;


        if (characters[index] == characters.Last())
            nextButton.interactable = false;
        else
            nextButton.interactable = true;

        if (buyed)
        {
            selectButton.gameObject.SetActive(true);
            buyButton.gameObject.SetActive(false);
        }
        else
        {
            selectButton.gameObject.SetActive(false);
            buyButton.gameObject.SetActive(true);
        }
    }

    public string GetSelectedPlayer()
    {
        if (currentCharacter == null)
            currentCharacter = characters[0];
        return currentCharacter.Name;
    }

    public void OnClickBuyButton()
    {
        if (MoneyController.inst.GetCoins() >= currentCharacter.Price)
        {
            var model = characters.FirstOrDefault(x => x.Name.Equals(currentCharacter.Name));
            PlayerPrefs.SetInt("Character" + currentCharacter.Name, 1);
            model.Buyed = true;
            MoneyController.inst.RemoveCoins(currentCharacter.Price);
            buyButton.gameObject.SetActive(false);
            selectButton.gameObject.SetActive(true);
        }
        else
        {
            MenuManager.inst.OnClickPurchaseWindow();
        }
    }

    public void OnClickSelectButton()
    {
        Debug.Log(currentCharacter);
        Debug.Log(currentCharacter.Name + "NAME");

        MenuManager.inst.OnClickSelectPlayer();
    }
}
