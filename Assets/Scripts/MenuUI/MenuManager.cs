﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : Singleton<MenuManager>
{
    public Camera canvasCamera;
    public Dictionary<MenuWindowName, MenuWindow> menuWindows;
    public MenuWindow menuButtonsWindow;
    public MenuWindow selectAreaWindow;
    public MenuWindow selecrPlayerWindow;
    public MenuWindow gameInterface;
    public MenuWindow gameOverWindow;
    public MenuWindow pauseWindow;
    public MenuWindow purchaseWindow;

    private MenuWindow currentMenu;
    private MenuWindow lastMenu;

    private void Start()
    {
        menuWindows = new Dictionary<MenuWindowName, MenuWindow>() { { MenuWindowName.MenuButton, menuButtonsWindow },
            { MenuWindowName.SelectArea, selectAreaWindow }, { MenuWindowName.SelectPlayer, selecrPlayerWindow}, { MenuWindowName.GameInterface, gameInterface },
            { MenuWindowName.GameOver, gameOverWindow }, { MenuWindowName.Pause, pauseWindow}, { MenuWindowName.Purchase, purchaseWindow} };

        InitCurrentMenu(null);
    }

    private void InitCurrentMenu(MenuWindow menu)
    {
        if (menu == null)
            currentMenu = menuWindows[0];
        else
        {
            lastMenu = currentMenu;
            currentMenu = menu;
        }

        currentMenu.Show();
    }

    public void OnClickPlayButton()
    {
        currentMenu.Hide();

        InitCurrentMenu(menuWindows[MenuWindowName.SelectArea]);
    }

    public void OnClickSelectPlayerButton()
    {
        currentMenu.Hide();

        InitCurrentMenu(menuWindows[MenuWindowName.SelectPlayer]);
    }

    public void OnClickSelectAreaButton()
    {
        currentMenu.Hide();

        InitCurrentMenu(menuWindows[MenuWindowName.GameInterface]);
    }

    public void OnClickBackButton()
    {
        currentMenu.Hide();

        InitCurrentMenu(lastMenu);
    }

    public void OnClickSelectPlayer()
    {
        currentMenu.Hide();

        InitCurrentMenu(menuWindows[MenuWindowName.MenuButton]);
    }

    public void MainMenu()
    {
        currentMenu.Hide();
        Camera.main.gameObject.SetActive(false);
        canvasCamera.gameObject.SetActive(true);
        InitCurrentMenu(menuWindows[MenuWindowName.MenuButton]);
    }

    public void GameOver()
    {
        currentMenu.Hide();

        InitCurrentMenu(menuWindows[MenuWindowName.GameOver]);
    }

    public void RestartGame()
    {
        currentMenu.Hide();

        InitCurrentMenu(menuWindows[MenuWindowName.GameInterface]);
        GameManager.inst.RestartGame();
    }

    public void PauseGame()
    {
        currentMenu.Hide();

        InitCurrentMenu(menuWindows[MenuWindowName.Pause]);
    }

    public void ResumeGame()
    {
        currentMenu.Hide();

        InitCurrentMenu(menuWindows[MenuWindowName.GameInterface]);
    }

    public void OnClickPurchaseWindow()
    {
        currentMenu.Hide();

        InitCurrentMenu(menuWindows[MenuWindowName.Purchase]);
    }
}

public enum MenuWindowName
{
    MenuButton = 0,
    SelectArea = 1,
    SelectPlayer = 2,
    GameInterface = 3,
    GameOver = 4,
    Pause = 5,
    Purchase = 6
}