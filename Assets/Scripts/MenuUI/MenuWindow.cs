﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuWindow : MonoBehaviour, IMenuWindow
{

    private AnimationMenuController animMenu;

    private void Awake()
    {
        animMenu = GetComponent<AnimationMenuController>();
    }

    public void Show()
    {
        gameObject.SetActive(true);
        animMenu.Show();
    }

    public void Hide()
    {
        animMenu.Hide();
    }
}
