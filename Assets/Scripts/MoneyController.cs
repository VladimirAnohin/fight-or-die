﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyController : Singleton<MoneyController>
{
    public MoneyView view;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        view.SetMoney(PlayerPrefs.GetInt("Coins", 0));
    }

    public void AddCoins(int coins)
    {
        int tempCoins = PlayerPrefs.GetInt("Coins", 0);
        coins = tempCoins + coins;
        if(view)
            view.SetMoney(coins);
        PlayerPrefs.SetInt("Coins", coins);
    }

    public void RemoveCoins(int coins)
    {
        int tempCoins = PlayerPrefs.GetInt("Coins", 0);
        coins = tempCoins - coins;
        if(view)
            view.SetMoney(coins);
        PlayerPrefs.SetInt("Coins", coins);
    }

    public int GetCoins()
    {
        return PlayerPrefs.GetInt("Coins", 0);
    }
}
