﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArenaController : MonoBehaviour
{
    public NpcSpawner spawner;
    public GameObject playerSpawn;
    public PlayerCamera playerCamera;

    private PlayerController player;
    private PlayerController currentPlayer;

    public void InstArena(PlayerController player)
    {
        Camera.main.gameObject.SetActive(false);

        InstPlayer(player);
        spawner.gameObject.SetActive(true);
    }

    private void InstPlayer(PlayerController player)
    {
        this.player = player;
        currentPlayer = Instantiate(player);
        currentPlayer.transform.position = playerSpawn.transform.position;
        playerCamera.gameObject.SetActive(true);
        playerCamera.SetTarget(currentPlayer.transform);
    }

    public void ClearPlayer()
    {
        Destroy(currentPlayer.gameObject);
    }

    public void ClearNpc()
    {
        spawner.ClearNpc();
        spawner.ResetCounter();
    }
}
