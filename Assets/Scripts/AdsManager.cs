﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsManager : MonoBehaviour
{

    private const string googlePlayGameId = "1798184";
    private const string iStoreGameId = "1798185";

    public string myPlacementId = "video";

    private bool testMode;

    private void Start()
    {
# if UNITY_EDITOR
        testMode = true;
#endif
        string currentModeId = googlePlayGameId;

#if UNITY_IOS
        currentModeId = iStoreGameId;
#endif

        Advertisement.Initialize(currentModeId);
    }

    [ContextMenu("ShowInterstetialAds")]
    public void ShowInterstetialAds()
    {
        if(Advertisement.IsReady())
            Advertisement.Show();
    }
}
