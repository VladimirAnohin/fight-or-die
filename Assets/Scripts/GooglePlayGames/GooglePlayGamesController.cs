﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using UnityEngine;

public class GooglePlayGamesController : MonoBehaviour
{
    public NpcSpawner npcSpawner;

    private bool isAuthorized;

    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();

        PlayGamesPlatform.InitializeInstance(config);
      //  // recommended for debugging:
        PlayGamesPlatform.DebugLogEnabled = true;
        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();

        Social.localUser.Authenticate((bool success) => {
            if (success)
            {
               // ((GooglePlayGames.PlayGamesPlatform)Social.Active).SetGravityForPopups(Gravity.BOTTOM);
                isAuthorized = true;
            }
        });
    }

    public void Rate()
    {
        Debug.Log(isAuthorized);
        if (isAuthorized)
            Social.ReportScore(PlayerPrefs.GetInt("MaxKills", 0), "CgkIo7m11v4HEAIQBw", (bool success) => {
            // handle success or failure
        });
    }

    public void ShowLeaderboard()
    {
        Debug.Log(isAuthorized);
        if(isAuthorized)
            PlayGamesPlatform.Instance.ShowLeaderboardUI("CgkIo7m11v4HEAIQBw");
    }
}
