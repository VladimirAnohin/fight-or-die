﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    [SerializeField]
    private int damage;
    private float timeBetweenAttack = 0.30f;

    private Collider hitTarget;

    private float timer = 0;
    private bool onCollision = false;
    private bool canHit;

    public void TryHit(float timer)
    {
        this.timer = timer;
        //canHit = true;
       // Hit();
    }

    public void StopHit(float timer)
    {
        //canHit = false;
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Npc") && canHit)
        {
            onCollision = true;
            hitTarget = collider;
            collider.gameObject.SendMessage("GetDamage", damage);
        }
    }

    private void Hit()
    {
        if (/*timer >= timeBetweenAttack &&*/ onCollision)
        {
            Debug.Log(hitTarget.gameObject.name);
            hitTarget.gameObject.SendMessage("GetDamage", damage);
            var a = hitTarget.gameObject.GetComponent<Rigidbody>();
            //if (a != null)
               // a.AddForce(Vector3.forward * 500, ForceMode.Force);
            onCollision = false;
        }
        timer = 0;
    }

    public void WeaponEffect()
    {
        var c = GetComponentInChildren<ParticleSystem>(true);
        c.Play();
    }
}
