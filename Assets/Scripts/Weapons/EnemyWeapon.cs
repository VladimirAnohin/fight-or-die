﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeapon : MonoBehaviour
{
    [SerializeField]
    private int damage;
    private bool isAtack = false;

    public void Hit(bool isAtack)
    {
        this.isAtack = isAtack;
    }

    public virtual void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player") && isAtack)
        {
            collider.gameObject.SendMessage("GetDamage", damage);
        }
        isAtack = false;
    }
}
