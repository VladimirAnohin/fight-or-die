﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using UnityEngine.AI;

public class NpcSpawner : MonoBehaviour
{
    public PlayerController playerController;
    public Enemy[] bots;
    public Transform[] spawnPoints;
    public int botsCount = 10;
    private int botsAmount = 0;
    public float timer = 0;
    public float timerUi = 0;
    private List<Enemy> spawnedBots = new List<Enemy>();
    private int killsCount = 0;
    public int KIllsCount { get { return killsCount; } }
    public float endTimer;
    public int endKills;

    public int spawnTimer;

    private void Start()
    {
        if (playerController == null)
            playerController = FindObjectOfType<PlayerController>();
    }

    void Update()
    {
        SpawnNpc();
        timer += Time.deltaTime;
        UpdateTimer();
    }

    private void SpawnNpc()
    {
        if (timer > spawnTimer)
        {
            int rand = Random.Range(0, spawnPoints.Count());
            int botIndex = Random.Range(0, spawnPoints.Count() - 1);
            var bot = Instantiate(bots[botIndex], spawnPoints[rand].transform.position, Quaternion.identity);
            bot.GetComponent<NavMeshAgent>().Warp(spawnPoints[rand].transform.position);
            bot.onDie += KillsCounter;
            spawnedBots.Add(bot);
            //bot.transform.SetParent(spawnPoints[rand].transform);
            botsAmount++;
            timer = 0;
        }
    }

    public void ClearNpc()
    {
        foreach (var bot in spawnedBots)
        {
            if (bot != null)
                Destroy(bot.gameObject);
        }

        spawnedBots.Clear();
    }

    public void ResetCounter()
    {
        killsCount = 0;
        timerUi = 0;
        PlayerUI.inst.UpdateCounter(killsCount);
    }

    private void KillsCounter(Enemy bot, int coins)
    {
        MoneyController.inst.AddCoins(coins);
        killsCount++;
        bot.onDie -= KillsCounter;
        PlayerUI.inst.UpdateCounter(killsCount);

        if (killsCount == 25)
            Social.ReportProgress("CgkIo7m11v4HEAIQAg", 100.0f, (bool success) =>
            {
                Debug.Log("SuccesKill25Tel");
            });

        if (playerController.IsDead)
        {
            endKills = killsCount;

            int savedMaxKills = PlayerPrefs.GetInt("MaxKills", 0);

            if (savedMaxKills < endKills)
                PlayerPrefs.SetInt("MaxKills", endKills);
        }
    }

    private void UpdateTimer()
    {
        timerUi += Time.deltaTime;
        PlayerUI.inst.UpdateTimer(timerUi);

        if (playerController.IsDead)
            endTimer = timerUi;
    }
}
