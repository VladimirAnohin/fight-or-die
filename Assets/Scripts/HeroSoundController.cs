﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroSoundController : MonoBehaviour
{

    private AudioSource source;

    public void PlaySource()
    {
        if (PlayerPrefs.GetInt("Sound") == 0)
            GetComponent<AudioSource>().Play();
    }
}
