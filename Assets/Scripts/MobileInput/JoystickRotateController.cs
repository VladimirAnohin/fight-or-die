﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class JoystickRotateController : Singleton<JoystickRotateController>, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    public Image image;
    public Image handlerImage;

    public JoystickButton joystickButtonAtack;
    public JoystickButton joystickButtonBlock;

    private Vector2 direction;
    private Vector2 center;
    public float imageExtens = 25f;

    private Quaternion rotation;

    private float angle;

    void Awake()
    {
        center = new Vector2(image.rectTransform.position.x, image.rectTransform.position.z);
        rotation = Quaternion.identity;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("PointerDown");
        //CalculateAngle(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        angle = -1;
        handlerImage.rectTransform.anchoredPosition = Vector2.zero;
        direction = handlerImage.rectTransform.anchoredPosition;
    }

    public void OnDrag(PointerEventData eventData)
    {
        CalculateAngle(eventData);
    }

    void CalculateAngle(PointerEventData eventData)
    {
        Vector3 pos = image.rectTransform.InverseTransformPoint(eventData.position);
        Vector2 goalPos = new Vector2(pos.x, pos.y);
        center = Vector3.zero;

        direction = center - goalPos;
        direction = direction.normalized;
        rotation = Quaternion.FromToRotation(new Vector3(0, 0, -1), new Vector3(direction.x, 0, direction.y));
        angle = rotation.eulerAngles.y;

        if (pos.magnitude > imageExtens)
        {
            pos = pos.normalized * imageExtens;
        }
        handlerImage.rectTransform.anchoredPosition = pos;
    }

    public Vector3 GetDirection()
    {
        Vector3 vec = new Vector3(direction.x, 0, direction.y);
        return vec;
    }

    public float GetAtackButton()
    {
        return joystickButtonAtack.GetButtonState();
    }

    public float GetBlockButton()
    {
        return joystickButtonBlock.GetButtonState();
    }
}
