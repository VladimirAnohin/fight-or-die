﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class JoystickButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private float pressed;

    public void OnPointerDown(PointerEventData eventData)
    {
        pressed = 1;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        pressed = 0;
    }

    public float GetButtonState()
    {
        return pressed;
    }
}
