﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshTest : MonoBehaviour
{
    private NavMeshAgent navMeshAgent;
    private Rigidbody rigidbody;
    private NavMeshPath path;
    private float elapsed = 0.0f;

    public Transform target;

    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        rigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        navMeshAgent.SetDestination(target.position);
        for (int i = 0; i < path.corners.Length - 1; i++)
            Debug.DrawLine(path.corners[i], path.corners[i + 1], Color.red);
    }
    public void Move(Vector3 position)
    {
        var dir = position - transform.position;

        rigidbody.MovePosition(transform.position + dir.normalized * Time.deltaTime * 5f);
    }
}
