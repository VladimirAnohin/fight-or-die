﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class Enemy : MonoBehaviour
{
    public ParticleSystem hitEffect;
    public Action<Enemy, int> onDie;

    private PlayerController target;
    private FSMSystem fsm;
    private AnimationController animationController;
    private EnemyWeapon currentWeapon;
    private EnemyMoving moving;
    private Rigidbody rigidbody;
    private NavMeshAgent navMeshAgent;

    public void SetTransition(Transition t) { fsm.PerformTransition(t); }

    [SerializeField]
    private int hp;
    public int Hp { get { return hp; } set { hp = value; } }
    [SerializeField]
    private int damage;
    [SerializeField]
    private float speed;
    [SerializeField]
    private int moneyReward;
    public int MoneyReward { get { return moneyReward; } }

    private void Awake()
    {
        if (target == null)
            target = FindObjectOfType<PlayerController>();

        target = FindObjectOfType<PlayerController>();
        animationController = GetComponent<AnimationController>();
        currentWeapon = transform.GetComponentInChildren<EnemyWeapon>(true);
        moving = GetComponent<EnemyMoving>();
        rigidbody = GetComponent<Rigidbody>();
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        MakeFSM();
    }

    private void FixedUpdate()
    {
        fsm.CurrentState.Reason(target, this);
        fsm.CurrentState.Act(target, this);
        Rotate();
    }

    private void MakeFSM()
    {
        MoveState move = new MoveState(moving, animationController, rigidbody, navMeshAgent);
        move.AddTransition(Transition.Atack, StateID.AtackID);
        move.AddTransition(Transition.Die, StateID.DieID);
        move.AddTransition(Transition.Idle, StateID.IdleID);

        AtackState atack = new AtackState(moving, animationController);
        atack.AddTransition(Transition.Move, StateID.MoveID);
        atack.AddTransition(Transition.Die, StateID.DieID);
        atack.AddTransition(Transition.Idle, StateID.IdleID);

        DeathState death = new DeathState(animationController, navMeshAgent);
        IdleState idle = new IdleState(animationController);

        fsm = new FSMSystem();

        fsm.AddState(move);
        fsm.AddState(atack);
        fsm.AddState(death);
        fsm.AddState(idle);
    }

    public void GetDamage(int damage)
    {
        if (fsm != null)
            if (fsm.CurrentStateID == StateID.DieID)
                return;

        hp -= damage;

        if (hitEffect != null)
            hitEffect.Play();
    }

    public void Atack()
    {
        currentWeapon.Hit(true);
        GetComponent<HeroSoundController>().PlaySource();
    }

    public void StartDeathAnimation()
    {
        StartCoroutine(DeathAnimation());
    }

    private void Rotate()
    {
        if (!rigidbody || hp <= 0)
            return;

        var direction = (target.transform.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(direction * Time.deltaTime);

        rigidbody.MoveRotation(lookRotation);
    }

    public IEnumerator DeathAnimation()
    {
        Destroy(GetComponent<Rigidbody>());
        GetComponent<CapsuleCollider>().enabled = false;
        yield return new WaitForSeconds(2f);
        Vector3 newPosition = new Vector3(transform.position.x, transform.position.y - 3, transform.position.z);
        transform.Translate(Vector3.down * 1f * Time.deltaTime);

        yield return new WaitForSeconds(1f);
        Destroy(gameObject);
    }
}

public class MoveState : FiniteStateMachine
{
    private AnimationController animationController;
    private EnemyMoving moving;
    private Rigidbody rigidbody;
    private NavMeshAgent agent;
    public Vector3 direction;

    public MoveState(EnemyMoving moving, AnimationController animationController, Rigidbody rigidbody, NavMeshAgent agent)
    {
        this.animationController = animationController;
        stateID = StateID.MoveID;
        this.moving = moving;
        this.rigidbody = rigidbody;
        this.agent = agent;
    }

    public override void Reason(PlayerController target, Enemy bot)
    {
        if (target == null)
            return;

        if (Vector3.Distance(moving.transform.position, target.transform.position) < moving.stopDistance)
        {
            animationController.Walk(false);
            animationController.SetIdle(true);
            bot.SetTransition(Transition.Atack);
        }

        if (bot.Hp <= 0)
        {
            animationController.Walk(false);
            bot.SetTransition(Transition.Die);
        }

        if (target.IsDead)
        {
            animationController.Walk(false);
            bot.SetTransition(Transition.Idle);
        }
    }

    public override void Act(PlayerController target, Enemy bot)
    {
        if (target == null)
            return;

        animationController.Walk(true);
        agent.SetDestination(target.transform.position);
        /* if (moving.Path.Count == 0)
             moving.FindPath(target.transform);

         if (moving.Path.Count > 0)
         {
             animationController.Walk(true);
             moving.Move(moving.Path[0]);
         }*/
    }
}


public class AtackState : FiniteStateMachine
{
    private AnimationController animationController;
    private EnemyMoving moving;

    public AtackState(EnemyMoving moving, AnimationController animationController)
    {
        this.animationController = animationController;
        stateID = StateID.AtackID;
        this.moving = moving;
    }

    public override void Reason(PlayerController player, Enemy bot)
    {
        if (Vector3.Distance(moving.transform.position, player.transform.position) > moving.stopDistance)
        {
            animationController.Atack(false);
            bot.SetTransition(Transition.Move);
        }

        if (bot.Hp <= 0)
            bot.SetTransition(Transition.Die);

        if (player.IsDead)
        {
            animationController.Atack(false);
            bot.SetTransition(Transition.Idle);
        }
    }

    public override void Act(PlayerController target, Enemy bot)
    {
        bot.Atack();
        animationController.Atack(true);
    }
}

public class DeathState : FiniteStateMachine
{
    private AnimationController animationController;
    private NavMeshAgent agent;

    public DeathState(AnimationController animationController, NavMeshAgent navMeshAgent)
    {
        this.animationController = animationController;
        agent = navMeshAgent;
        stateID = StateID.DieID;
    }

    public override void Reason(PlayerController target, Enemy bot)
    {
    }

    public override void Act(PlayerController target, Enemy bot)
    {
        agent.enabled = false;
        animationController.Death(true);
        //TODO Доделать грамотный поворот коллайдера при смерти
        //bot.GetComponent<CapsuleCollider>().direction = 2;
        bot.StartDeathAnimation();

        if (bot.onDie != null)
            bot.onDie(bot, bot.MoneyReward);
    }
}

public class IdleState : FiniteStateMachine
{
    private AnimationController animationController;

    public IdleState(AnimationController animationController)
    {
        this.animationController = animationController;
        stateID = StateID.IdleID;
    }

    public override void Reason(PlayerController target, Enemy bot)
    {
    }

    public override void Act(PlayerController target, Enemy bot)
    {
        animationController.SetIdle(true);
    }
}