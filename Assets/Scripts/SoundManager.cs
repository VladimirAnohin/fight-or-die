﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager>
{

    public AudioSource mainMenuSource;

    private void Start()
    {
        StartMenuMusic();
    }

    public void StartMenuMusic()
    {
        mainMenuSource.Play();
    }

    public void StopMenu()
    {
        if (mainMenuSource.isPlaying)
            mainMenuSource.Stop();
    }

    public void OnOffMusic()
    {
        int sound = PlayerPrefs.GetInt("Sound", 1);

        if (sound == 1)
        {
            PlayerPrefs.SetInt("Sound", 0);
            sound = 0;
        }
        else
        {
            PlayerPrefs.SetInt("Sound", 1);
            sound = 1;
        }
        Debug.Log(PlayerPrefs.GetInt("Sound", 1) + "/" + sound);
        mainMenuSource.mute = System.Convert.ToBoolean(sound);
    }
}
